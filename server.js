var express = require('express')
var cors = require('cors')
const mysql = require('mysql2');
const PORT = process.env.PORT || 5000
const connection = mysql.createConnection({
host: '157.245.59.56',
port: '3366',
user: '6300472',
password: '6300472',
database: '6300472'
});
var app = express()
app.use(cors())
app.use(express.json())

app.get('/travel', function (req, res, next) {
  connection.query(
    'SELECT * FROM `travel`',
    function(err, results, fields) {
      res.json(results);
    }
  );
})

app.get('/travel/:id', function (req, res, next) {
  const id = req.params.id;
  connection.query(
    'SELECT * FROM `travel` WHERE `id` = ?',
    [id],
    function(err, results) {
      res.json(results);
    }
  );
})

app.post('/travel', function (req, res, next) {
  connection.query(
    'INSERT INTO `travel`(`name`, `time`, `type`, `price`, `avatar`) VALUES (?, ?, ?, ?, ?)',
    [req.body.name, req.body.time, req.body.type, req.body.price, req.body.avatar],
    function(err, results) {
      res.json(results);
    }
  );
})

app.put('/travel', function (req, res, next) {
  connection.query(
    'UPDATE `travel` SET `name`= ?, `time`= ?, `type`= ?, `price`= ?, `avatar`= ? WHERE id = ?',
    [req.body.name, req.body.time, req.body.type, req.body.price, req.body.avatar, req.body.id],
    function(err, results) {
      res.json(results);
    }
  );
})

app.delete('/travel', function (req, res, next) {
  connection.query(
    'DELETE FROM `travel` WHERE id = ?',
    [req.body.id],
    function(err, results) {
      res.json(results);
    }
  );
})

app.listen(PORT, function () {
  console.log('CORS-enabled web server listening on port '+PORT)
})
